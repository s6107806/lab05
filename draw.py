import numpy as np
def draw_circle( img , center_x, center_y,radius,intensity ):
    m = img
    x = center_x
    y = center_y
    r = radius
    int = intensity
    for i in range (0,img.shape[0]):
        for j in range (0,img.shape[1]):
            if(i-x)**2+(j-y)**2<=r**2:
                m[i][j]=int
    return m
